﻿Imports System.Text
Imports System.IO
Imports System.Collections.Generic
Imports Npgsql
Imports System.Text.RegularExpressions
Imports System.Linq
Imports System.Xml.Linq
Imports System.Xml.Schema

Public Class Form1
    Public Const SOFT_VERSION = "Site[cars245] daily sync tool v3.1"
    Public Const CONNECTIONSTRING = "server=192.168.1.5;userid=cars245;password=cars245;database=autobodyshop;port=5432" _
                                    + ";CommandTimeout=0" _
                                    + ";ApplicationName=" + SOFT_VERSION _
                                    + ";pooling=false" _
                                    + ";SearchPath=cars245,tdr,plv,public"
    Public Const FILEPATH = "C:\TDR\Export files\"
    Public conn As New NpgsqlConnection(CONNECTIONSTRING)

    Private Sub SplitXmlFile(sourceFile As String _
                        , rootElement As String _
                        , descendantElement As String _
                        , takeElements As Integer _
                        , destFilePrefix As String _
                        , destPath As String)

        Dim Xml As XElement = XElement.Load(sourceFile)
        ' Child elements from source file to split by.
        Dim childNodes = Xml.Descendants(descendantElement)
        ' This is the total number of elements to be sliced up into 
        ' separate files.
        Dim cnt As Int64 = childNodes.Count()

        Dim skip = 0
        Dim take = takeElements
        Dim fileno = 0

        ' Split elements into chunks and save to disk.
        While (skip < cnt)
            ' Extract portion of the xml elements.
            Dim c1 = childNodes _
            .Skip(skip) _
            .Take(take)

            ' Setup number of elements to skip on next iteration.
            skip += take
            ' File sequence no for split file.
            fileno += 1
            ' Filename for split file.
            Dim filename = String.Format(destFilePrefix + "_part{0}.xml", fileno)
            ' Create a partial xml document.
            Dim xelem As XElement = New XElement(rootElement, c1)
            Dim frag As XElement = New XElement("cars245", xelem)
            ' Save to disk.
            frag.Save(destPath + filename)
        End While

    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim sbResult As New StringBuilder
        Dim counter As Int64 = 0
        Dim chunkSize As Integer
        Dim products As New List(Of String)
        Dim sFileName As String
        Dim Reader As NpgsqlDataReader
        Dim Command As New NpgsqlCommand
        Dim syncDate As String = DateTime.Now.ToString("yyyy-MM-dd_HH-mm")


        tb_chunkSize.Enabled = False
        chunkSize = Convert.ToInt32(tb_chunkSize.Text)

        Application.DoEvents()
        Try
            products.Clear()

            For Each itemChecked In CheckedListBox1.CheckedItems
                products.Add(itemChecked.ToString)
            Next

            Command.Connection = conn

            For i = 0 To products.Count - 1
                Label2.Text = products.Item(i).ToString & " - in process..."
                Command.CommandText = "begin;"
                Command.ExecuteNonQuery()
                Command.CommandText = "select * from cars245.product_data_sync_cursor('" + products.Item(i) + "','product_data_sync_cursor');"
                Command.ExecuteNonQuery()
                counter = 0
                Do
                    Command.CommandText = "fetch forward " + chunkSize.ToString() + " from product_data_sync_cursor;"
                    Reader = Command.ExecuteReader
                    sbResult.Clear()
                    If Reader.HasRows Then
                        While Reader.Read()
                            sbResult.Append(Reader.Item(0).ToString)
                        End While
                        Reader.Close()
                        counter += chunkSize
                        sFileName = "\CARS245_" & products.Item(i).Replace("site_pl_", "") & "_sync" +
                            "_part" + (counter \ chunkSize).ToString() +
                            "_" + syncDate +
                            ".xml"
                        File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245>")
                        File.AppendAllText(FILEPATH + sFileName, sbResult.ToString)
                        File.AppendAllText(FILEPATH + sFileName, "</cars245>")
                    Else
                        Reader.Close()
                        Command.CommandText = "close product_data_sync_cursor;"
                        Command.ExecuteNonQuery()
                        Command.CommandText = "commit;"
                        Command.ExecuteNonQuery()
                        Exit Do
                    End If
                Loop
                sbResult.Clear()
                Label1.Text &= vbCrLf & products.Item(i).ToString & " - done"
                Application.DoEvents()
            Next i
        Catch ex As Exception
            MessageBox.Show(ex.Message & vbCrLf & ex.ToString)
        End Try
        MsgBox("done")
        tb_chunkSize.Enabled = True
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = SOFT_VERSION
        Try
            conn.Open()
        Catch ex As Exception
            MessageBox.Show(ex.Message & vbCrLf & ex.ToString)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btn_Get_Config.Click
        Dim Reader As NpgsqlDataReader
        Dim Command As New NpgsqlCommand
        Dim products As New List(Of String)

        products.Clear()
        CheckedListBox1.Items.Clear()

        Try
            Command.CommandText = "select table_name " +
                                    "from cars245.pl_info " +
                                    "where is_active " +
                                    "order by 1;"

            Command.Connection = conn
            Reader = Command.ExecuteReader
            While Reader.Read
                products.Add(Reader.Item(0).ToString)
            End While
            Reader.Close()

            For i = 0 To products.Count - 1
                CheckedListBox1.Items.Add(products.Item(i).ToString)
                CheckedListBox1.SetItemChecked(i, True)
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message & vbCrLf & ex.ToString)
        End Try

        Button1.Enabled = True
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        For i = 0 To CheckedListBox1.Items.Count - 1
            CheckedListBox1.SetItemChecked(i, True)
        Next
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        For i = 0 To CheckedListBox1.Items.Count - 1
            CheckedListBox1.SetItemChecked(i, False)
        Next
    End Sub

    Private Sub Form1_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            conn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message & vbCrLf & ex.ToString)
        End Try
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim sbResult As New StringBuilder
        Dim sFileName As String
        'Dim Reader As NpgsqlDataReader
        Dim Limit As Integer
        Dim Offset As Integer
        Dim ok As Boolean

        Try
            Limit = 25000
            Offset = 0
            ok = True

            While ok
                Dim Command As New NpgsqlCommand
                Command.Connection = conn
                Command.CommandTimeout = 1024
                Command.CommandText = "" +
                    "SELECT id, XMLELEMENT(NAME ""discounts"", XMLCONCAT(data_block,item_block)) as ""discounts""                            " +
                    "FROM (                                                                                                                  " +
                    "	SELECT d.id                                                                                                          " +
                    "		,XMLELEMENT(NAME ""data""                                                                                        " +
                    "						,XMLELEMENT(NAME ""from"", valid_from)                                                           " +
                    "						,XMLELEMENT(NAME ""till"", valid_untill)                                                         " +
                    "						,XMLELEMENT(NAME ""percent"", ""percent"")                                                       " +
                    "		) as data_block                                                                                                  " +
                    "		,XMLELEMENT(NAME ""items"", 	(SELECT XMLAGG(q)                                                                " +
                    "									FROM (                                                                               " +
                    "										SELECT XMLELEMENT(NAME ""id"", XMLATTRIBUTES(product_id as ""id""))  as q        " +
                    "										FROM discounted_products as dp                                                   " +
                    "										WHERE discount_id = d.id                                                         " +
                    "										ORDER by discount_id                                                             " +
                    "										LIMIT " + Limit.ToString + " OFFSET " + Offset.ToString + "                      " +
                    "										) as z                                                                           " +
                    "									)                                                                                    " +
                    "		) as item_block                                                                                                  " +
                    "                                                                                                                        " +
                    "	FROM discounts AS d                                                                                                  " +
                    ") as tmp                                                                                                                " +
                    "WHERE XMLEXISTS('//id' PASSING BY REF item_block);                                                                      "

                Dim Reader = Command.ExecuteReader()
                While Reader.Read()
                    sbResult.Clear()
                    sbResult.Append(Reader.Item(1).ToString)
                    sFileName = "\CARS245_discount_group" + Reader.Item(0).ToString + "_OFFSET" + Offset.ToString + "_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm") + ".xml"
                    File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245>")
                    File.AppendAllText(FILEPATH + sFileName, sbResult.ToString)
                    File.AppendAllText(FILEPATH + sFileName, "</cars245>")
                End While

                If Reader.HasRows Then
                    Offset += Limit
                Else
                    ok = False
                End If
                Reader.Close()

            End While
        Catch ex As Exception
            MessageBox.Show(ex.Message & vbCrLf & ex.ToString)
        End Try
        MsgBox("discounts - done")
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles get_ws_list.Click
        Dim Reader As NpgsqlDataReader
        Dim Command As New NpgsqlCommand
        Dim clients As New List(Of String)
        Dim slugs As New List(Of String)

        clients.Clear()
        CheckedListBoxClients.Items.Clear()

        ListBoxClientsInfoBlock.Items.Clear()

        ListBoxBrandsInfoBlock.Items.Clear()

        slugs.Clear()
        CheckedListBoxSlugs.Items.Clear()


        Try
            'clients:
            Command.CommandText = "select id from clients order by 1;"
            Command.Connection = conn
            Reader = Command.ExecuteReader
            While Reader.Read
                clients.Add(Reader.Item(0).ToString)
            End While
            Reader.Close()
            For i = 0 To clients.Count - 1
                CheckedListBoxClients.Items.Add(clients.Item(i).ToString)
                CheckedListBoxClients.SetItemChecked(i, True)
                If Regex.IsMatch(clients.Item(i).ToString, "2808", RegexOptions.IgnoreCase) Then
                    CheckedListBoxClients.SetItemChecked(i, False)
                End If
            Next

            'clients info block:
            Command.CommandText = "select id::text || ' => ' || name from clients order by 1;"
            Command.Connection = conn
            Reader = Command.ExecuteReader
            While Reader.Read
                ListBoxClientsInfoBlock.Items.Add(Reader.Item(0).ToString)
            End While
            Reader.Close()

            'brands info block:
            Command.CommandText = "select left(""slug-"",-1) || ' => ' || (select reltuples from pg_class where relname = ""table_name"") :: text as approximate_count " +
                    "from pl_info " +
                    "where ""slug-"" is not null " +
                    "order by  1 "
            Command.Connection = conn
            Reader = Command.ExecuteReader
            While Reader.Read
                ListBoxBrandsInfoBlock.Items.Add(Reader.Item(0).ToString)
            End While
            Reader.Close()

            'slugs:
            Command.CommandText = "select ""slug-"" from slugs order by 1;"
            Reader = Command.ExecuteReader
            While Reader.Read
                slugs.Add(Reader.Item(0).ToString)
            End While
            Reader.Close()
            For i = 0 To slugs.Count - 1
                CheckedListBoxSlugs.Items.Add(slugs.Item(i).ToString)
                CheckedListBoxSlugs.SetItemChecked(i, False)
            Next


        Catch ex As Exception
            MessageBox.Show(ex.Message & vbCrLf & ex.ToString)
        End Try

        btn_ws_generate.Enabled = True
    End Sub




    Private Sub btn_ws_select_all_Click(sender As Object, e As EventArgs) Handles btn_ws_select_all.Click
        For i = 0 To CheckedListBoxClients.Items.Count - 1
            CheckedListBoxClients.SetItemChecked(i, True)
        Next
    End Sub

    Private Sub btn_ws_remove_selection_Click(sender As Object, e As EventArgs) Handles btn_ws_remove_selection.Click
        For i = 0 To CheckedListBoxClients.Items.Count - 1
            CheckedListBoxClients.SetItemChecked(i, False)
        Next
    End Sub

    Private Sub btn_ws_generate_Click(sender As Object, e As EventArgs) Handles btn_ws_generate.Click
        Dim sbResult As New StringBuilder
        Dim count As Integer = 0
        Dim counter As Integer = 0
        Dim clients As New List(Of String)
        Dim sFileName As String
        Dim Reader As NpgsqlDataReader
        Dim Command As New NpgsqlCommand
        Dim slugs As String
        Dim brands As String

        Application.DoEvents()
        Try
            clients.Clear()

            For Each itemChecked In CheckedListBoxClients.CheckedItems
                clients.Add(itemChecked.ToString)
            Next

            Command.Connection = conn

            For i = 0 To clients.Count - 1
                Label5.Text = clients.Item(i).ToString & " - in process..."
                Application.DoEvents()

                count = 0
                counter = 0
                slugs = "("
                brands = ""
                For Each itm In CheckedListBoxSlugs.CheckedItems
                    slugs += ",'" + itm.ToString + "'"
                    brands += itm.ToString
                Next
                slugs = slugs.Replace("(,", "(")
                slugs += ")"
                If slugs = "()" Then
                    slugs = ""
                    brands = "all_"
                Else
                    slugs = " and ""slug-"" in " + slugs
                End If

                sFileName = "\CARS245_" & clients.Item(i) & "_client_discounts[upd]_" + brands + DateTime.Now.ToString("dd-MM-yyyy_HH-mm") + ".xml"
                File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245><discounts>")

                Command.CommandText = "select count(*) from clients_discounts where client_id = " + clients.Item(i) + slugs
                count = Command.ExecuteScalar()
                Debug.Print(clients.Item(i) + "-----" + count.ToString)

                While counter < count
                    Command.CommandText = "	SELECT 	XMLELEMENT(name discount " +
                                        "				,XMLELEMENT(name user, client_id)" +
                                        "				,XMLELEMENT(name from, to_char(valid_from,'YYYY.MM.DD HH24:MI:SS'))" +
                                        "				,XMLELEMENT(name till, to_char(valid_till,'YYYY.MM.DD HH24:MI:SS'))" +
                                        "				,XMLELEMENT(name product, XMLATTRIBUTES(""slug-"" || parsed_sku AS id))" +
                                        "				,CASE " +
                                        "					WHEN discount_percent IS NOT NULL THEN " +
                                        "						XMLELEMENT(name percent, discount_percent)" +
                                        "					ELSE " +
                                        "						XMLELEMENT(name prices, XMLELEMENT(name price, XMLATTRIBUTES(discount_currency AS currency),discount_fixed_price))" +
                                        "				END " +
                                        "			) as xml_body" +
                                        "	FROM ( SELECT * FROM clients_discounts WHERE client_id = " + clients.Item(i) + slugs + " ORDER BY id LIMIT 20000 OFFSET " + counter.ToString + ") as q;"


                    Reader = Command.ExecuteReader
                    sbResult.Clear()
                    While Reader.Read
                        sbResult.Append(Reader.Item(0).ToString)
                    End While
                    Reader.Close()
                    counter += 20000
                    File.AppendAllText(FILEPATH + sFileName, sbResult.ToString)
                End While

                File.AppendAllText(FILEPATH + sFileName, "</discounts></cars245>")
                Label4.Text &= vbCrLf & clients.Item(i).ToString & " - done"
                Label5.Text = ""
                Application.DoEvents()
            Next i
        Catch ex As Exception
            MessageBox.Show(ex.Message & vbCrLf & ex.ToString)
        End Try
        MsgBox("clients discounts[UPD] - done")
    End Sub



    Private Sub CheckBox_del_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox_del.CheckedChanged
        If CheckBox_del.Checked Then
            btn_ws_delete_client_discounts.Enabled = True
        Else : btn_ws_delete_client_discounts.Enabled = False
        End If
    End Sub

    Private Sub btn_ws_delete_client_discounts_Click(sender As Object, e As EventArgs) Handles btn_ws_delete_client_discounts.Click
        Dim sbResult As New StringBuilder
        Dim count As Integer = 0
        Dim counter As Integer = 0
        Dim clients As New List(Of String)
        Dim sFileName As String
        Dim Reader As NpgsqlDataReader
        Dim Command As New NpgsqlCommand
        Dim slugs As String
        Dim brands As String

        Application.DoEvents()
        Try
            clients.Clear()

            For Each itemChecked In CheckedListBoxClients.CheckedItems
                clients.Add(itemChecked.ToString)
            Next

            Command.Connection = conn

            For i = 0 To clients.Count - 1
                Label5.Text = clients.Item(i).ToString & " - in process..."
                Application.DoEvents()

                count = 0
                counter = 0
                slugs = "("
                brands = ""
                For Each itm In CheckedListBoxSlugs.CheckedItems
                    slugs += ",'" + itm.ToString + "'"
                    brands += itm.ToString
                Next
                slugs = slugs.Replace("(,", "(")
                slugs += ")"
                If slugs = "()" Then
                    slugs = ""
                    brands = "all_"
                Else
                    slugs = " and ""slug-"" in " + slugs
                End If

                sFileName = "\CARS245_" & clients.Item(i) & "_client_discounts[del]_" + brands + DateTime.Now.ToString("dd-MM-yyyy_HH-mm") + ".xml"
                File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245><discounts>")

                Command.CommandText = "select count(*) from clients_discounts where client_id = " + clients.Item(i) + slugs
                count = Command.ExecuteScalar()
                Debug.Print(clients.Item(i) + "-----" + count.ToString)

                While counter < count
                    Command.CommandText = "	SELECT 	XMLELEMENT(name discount " +
                                        "				,XMLELEMENT(name user, client_id)" +
                                        "				,XMLELEMENT(name product, XMLATTRIBUTES(""slug-"" || parsed_sku AS id))" +
                                        "			) as xml_body" +
                                        "	FROM ( SELECT * FROM clients_discounts WHERE client_id = " + clients.Item(i) + slugs + " ORDER BY id LIMIT 20000 OFFSET " + counter.ToString + ") as q;"
                    Reader = Command.ExecuteReader
                    sbResult.Clear()
                    While Reader.Read
                        sbResult.Append(Reader.Item(0).ToString)
                    End While
                    Reader.Close()
                    counter += 20000
                    File.AppendAllText(FILEPATH + sFileName, sbResult.ToString)
                End While

                File.AppendAllText(FILEPATH + sFileName, "</discounts></cars245>")
                Label4.Text &= vbCrLf & clients.Item(i).ToString & " - done"
                Label5.Text = ""
                Application.DoEvents()
            Next i
        Catch ex As Exception
            MessageBox.Show(ex.Message & vbCrLf & ex.ToString)
        End Try
        MsgBox("clients discounts[DEL] - done")
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        For i = 0 To CheckedListBoxSlugs.Items.Count - 1
            CheckedListBoxSlugs.SetItemChecked(i, True)
        Next
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        For i = 0 To CheckedListBoxSlugs.Items.Count - 1
            CheckedListBoxSlugs.SetItemChecked(i, False)
        Next
    End Sub

    Private Sub Button6_Click_1(sender As Object, e As EventArgs) Handles Button6.Click
        Dim sFileName As String
        Dim Reader As NpgsqlDataReader
        Dim Command As New NpgsqlCommand
        Dim sbResult As New StringBuilder
        Dim counter As Long
        Dim cursor As String
        Dim cursor2 As String
        'params for SplitXmlFile()
        Dim sourceFile As String
        Dim rootElement As String
        Dim descElement As String
        Dim take As Integer = Convert.ToInt32(tb_chunkSize_ws.Text)
        Dim chunk As Integer = Convert.ToInt32(tb_chunkSize_ws.Text)
        Dim cursor_chunk As Integer
        Dim part_counter As Integer
        Dim sync_date As String = DateTime.Now.ToString("dd-MM-yyyy_HH-mm")
        Dim destFilePrefix As String
        Dim destPath As String = FILEPATH

        Try
            tb_chunkSize_ws.Enabled = False
            'UPD part begin--------------------------------------------------------------------

            'b2b v1: clients_discounts - personal ws. prices
            part_counter = 1
            counter = 10000
            cursor_chunk = 10000
            sFileName = "b2b_v1_sync_upd_" + sync_date + "_part" + part_counter.ToString() + ".xml"
            File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245><discounts>")
            Command.Connection = New NpgsqlConnection(CONNECTIONSTRING)
            Command.Connection.Open()
            Command.CommandText = "BEGIN ;"
            Command.ExecuteNonQuery()
            cursor = Guid.NewGuid.ToString
            Command.CommandText = "SELECT * from ws_daily_sync_upd('" + cursor + "');"
            Command.ExecuteNonQuery()
            Do
                Command.CommandText = "FETCH FORWARD " + cursor_chunk.ToString + " FROM """ + cursor + """;"
                Reader = Command.ExecuteReader
                sbResult.Clear()
                If Reader.HasRows Then
                    While Reader.Read()
                        sbResult.Append(Reader.Item(0).ToString)
                    End While
                    Reader.Close()
                    If counter Mod chunk = 0 Then
                        File.AppendAllText(FILEPATH + sFileName, "</discounts></cars245>")
                        part_counter += 1
                        sFileName = "b2b_v1_sync_upd_" + sync_date + "_part" + part_counter.ToString() + ".xml"
                        File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245><discounts>")
                    End If
                    counter += cursor_chunk
                    File.AppendAllText(FILEPATH + sFileName, sbResult.ToString)
                Else
                    Reader.Close()
                    Command.CommandText = "COMMIT;"
                    Command.ExecuteNonQuery()
                    Command.Cancel()
                    Command.Connection.Close()
                    File.AppendAllText(FILEPATH + sFileName, "</discounts></cars245>")
                    Exit Do
                End If
            Loop

            'part2: full presets
            part_counter = 1
            counter = 10000
            cursor_chunk = 10000
            sFileName = "b2b_v2_sync_upd_" + sync_date + "_part" + part_counter.ToString() + ".xml"
            File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245><discounts>")
            Command.Connection = New NpgsqlConnection(CONNECTIONSTRING)
            Command.Connection.Open()
            Command.CommandText = "BEGIN ;"
            Command.ExecuteNonQuery()
            cursor = Guid.NewGuid.ToString
            Command.CommandText = "SELECT * from ws_daily_full_preset_sync_upd('" + cursor + "');"
            Command.ExecuteNonQuery()
            Do
                Command.CommandText = "FETCH FORWARD " + cursor_chunk.ToString + " FROM """ + cursor + """;"
                Reader = Command.ExecuteReader
                sbResult.Clear()
                If Reader.HasRows Then
                    While Reader.Read()
                        sbResult.Append(Reader.Item(0).ToString)
                    End While
                    Reader.Close()
                    If counter Mod chunk = 0 Then
                        File.AppendAllText(FILEPATH + sFileName, "</discounts></cars245>")
                        part_counter += 1
                        sFileName = "b2b_v2_sync_upd_" + sync_date + "_part" + part_counter.ToString() + ".xml"
                        File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245><discounts>")
                    End If
                    counter += cursor_chunk
                    File.AppendAllText(FILEPATH + sFileName, sbResult.ToString)
                Else
                    Reader.Close()
                    Command.CommandText = "COMMIT;"
                    Command.ExecuteNonQuery()
                    Command.Cancel()
                    Command.Connection.Close()
                    File.AppendAllText(FILEPATH + sFileName, "</discounts></cars245>")
                    Exit Do
                End If
            Loop

            'UPD part end----------------------------------------------------------------------

            'INS part begin--------------------------------------------------------------------

            'part1: clients_discounts - personal ws. prices
            cursor_chunk = 10000
            counter = 10000
            part_counter = 1
            sFileName = "b2b_v1_sync_ins_" + sync_date + "_part" + part_counter.ToString() + ".xml"
            File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245><discounts>")
            Command.Connection = New NpgsqlConnection(CONNECTIONSTRING)
            Command.Connection.Open()
            Command.CommandText = "BEGIN ;"
            Command.ExecuteNonQuery()
            cursor = Guid.NewGuid.ToString
            Command.CommandText = "SELECT * from ws_daily_sync_ins('" + cursor + "');"
            Command.ExecuteNonQuery()
            Do
                Command.CommandText = "FETCH FORWARD " + cursor_chunk.ToString + " FROM """ + cursor + """;"
                Reader = Command.ExecuteReader
                sbResult.Clear()
                If Reader.HasRows Then
                    While Reader.Read()
                        sbResult.Append(Reader.Item(0).ToString)
                    End While
                    Reader.Close()
                    If counter Mod chunk = 0 Then
                        File.AppendAllText(FILEPATH + sFileName, "</discounts></cars245>")
                        part_counter += 1
                        sFileName = "b2b_v1_sync_ins_" + sync_date + "_part" + part_counter.ToString() + ".xml"
                        File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245><discounts>")
                    End If
                    counter += cursor_chunk
                    File.AppendAllText(FILEPATH + sFileName, sbResult.ToString)
                Else
                    Reader.Close()
                    Command.CommandText = "COMMIT;"
                    Command.ExecuteNonQuery()
                    Command.Cancel()
                    Command.Connection.Close()
                    File.AppendAllText(FILEPATH + sFileName, "</discounts></cars245>")
                    Exit Do
                End If
            Loop

            'part2: full presets
            part_counter = 1
            counter = 10000
            cursor_chunk = 10000
            sFileName = "b2b_v2_sync_ins_" + sync_date + "_part" + part_counter.ToString() + ".xml"
            File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245><discounts>")
            Command.Connection = New NpgsqlConnection(CONNECTIONSTRING)
            Command.Connection.Open()
            Command.CommandText = "BEGIN ;"
            Command.ExecuteNonQuery()
            cursor = Guid.NewGuid.ToString
            Command.CommandText = "SELECT * from ws_daily_full_preset_sync_ins('" + cursor + "');"
            Command.ExecuteNonQuery()
            Do
                Command.CommandText = "FETCH FORWARD " + cursor_chunk.ToString + " FROM """ + cursor + """;"
                Reader = Command.ExecuteReader
                sbResult.Clear()
                If Reader.HasRows Then
                    While Reader.Read()
                        sbResult.Append(Reader.Item(0).ToString)
                    End While
                    Reader.Close()
                    If counter Mod chunk = 0 Then
                        File.AppendAllText(FILEPATH + sFileName, "</discounts></cars245>")
                        part_counter += 1
                        sFileName = "b2b_v2_sync_ins_" + sync_date + "_part" + part_counter.ToString() + ".xml"
                        File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245><discounts>")
                    End If
                    counter += cursor_chunk
                    File.AppendAllText(FILEPATH + sFileName, sbResult.ToString)
                Else
                    Reader.Close()
                    Command.CommandText = "COMMIT;"
                    Command.ExecuteNonQuery()
                    Command.Cancel()
                    Command.Connection.Close()
                    File.AppendAllText(FILEPATH + sFileName, "</discounts></cars245>")
                    Exit Do
                End If
            Loop
            'INS part end----------------------------------------------------------------------

            'DEL part begin--------------------------------------------------------------------

            'part1: clients_discounts - personal ws. prices
            part_counter = 1
            counter = 10000
            cursor_chunk = 10000
            sFileName = "b2b_v1_sync_del_" + sync_date + "_part" + part_counter.ToString() + ".xml"
            File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245><discounts>")
            Command.Connection = New NpgsqlConnection(CONNECTIONSTRING)
            Command.Connection.Open()
            Command.CommandText = "BEGIN ;"
            Command.ExecuteNonQuery()
            cursor = Guid.NewGuid.ToString
            Command.CommandText = "SELECT * from ws_daily_sync_del('" + cursor + "');"
            Command.ExecuteNonQuery()
            Do
                Command.CommandText = "FETCH FORWARD " + cursor_chunk.ToString + " FROM """ + cursor + """;"
                Reader = Command.ExecuteReader
                sbResult.Clear()
                If Reader.HasRows Then
                    While Reader.Read()
                        sbResult.Append(Reader.Item(0).ToString)
                    End While
                    Reader.Close()
                    If counter Mod chunk = 0 Then
                        File.AppendAllText(FILEPATH + sFileName, "</discounts></cars245>")
                        part_counter += 1
                        sFileName = "b2b_v1_sync_del_" + sync_date + "_part" + part_counter.ToString() + ".xml"
                        File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245><discounts>")
                    End If
                    counter += cursor_chunk
                    File.AppendAllText(FILEPATH + sFileName, sbResult.ToString)
                Else
                    Reader.Close()
                    Command.CommandText = "COMMIT;"
                    Command.ExecuteNonQuery()
                    Command.Cancel()
                    Command.Connection.Close()
                    File.AppendAllText(FILEPATH + sFileName, "</discounts></cars245>")
                    Exit Do
                End If
            Loop

            'part2: full presets
            part_counter = 1
            counter = 10000
            cursor_chunk = 10000
            sFileName = "b2b_v2_sync_del_" + sync_date + "_part" + part_counter.ToString() + ".xml"
            File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245><discounts>")
            Command.Connection = New NpgsqlConnection(CONNECTIONSTRING)
            Command.Connection.Open()
            Command.CommandText = "BEGIN ;"
            Command.ExecuteNonQuery()
            cursor = Guid.NewGuid.ToString
            Command.CommandText = "SELECT * from ws_daily_full_preset_sync_del('" + cursor + "');"
            Command.ExecuteNonQuery()
            Do
                Command.CommandText = "FETCH FORWARD " + cursor_chunk.ToString + " FROM """ + cursor + """;"
                Reader = Command.ExecuteReader
                sbResult.Clear()
                If Reader.HasRows Then
                    While Reader.Read()
                        sbResult.Append(Reader.Item(0).ToString)
                    End While
                    Reader.Close()
                    If counter Mod chunk = 0 Then
                        File.AppendAllText(FILEPATH + sFileName, "</discounts></cars245>")
                        part_counter += 1
                        sFileName = "b2b_v2_sync_del_" + sync_date + "_part" + part_counter.ToString() + ".xml"
                        File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245><discounts>")
                    End If
                    counter += cursor_chunk
                    File.AppendAllText(FILEPATH + sFileName, sbResult.ToString)
                Else
                    Reader.Close()
                    Command.CommandText = "COMMIT;"
                    Command.ExecuteNonQuery()
                    Command.Cancel()
                    Command.Connection.Close()
                    File.AppendAllText(FILEPATH + sFileName, "</discounts></cars245>")
                    Exit Do
                End If
            Loop

            'UPD part end--------------------------------------------------------------------

            'b2b v3.0 and product_data (diff) --------------------------------------------------------------------
            cursor_chunk = 10000
            counter = 10000
            part_counter = 1
            sFileName = "b2b_v3_and_product_data_sync_" + sync_date + "_part" + part_counter.ToString() + ".xml"
            File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245>")
            cursor = Guid.NewGuid.ToString
            cursor2 = Guid.NewGuid.ToString
            Command.Connection = New NpgsqlConnection(CONNECTIONSTRING)
            Command.Connection.Open()
            Command.CommandText = "BEGIN ;"
            Command.ExecuteNonQuery()
            Command.CommandText = "select cars245.b2b_and_product_data_merge(" _
                                        + " (select cars245.b2b_v3_purchase_prices_sync('" + cursor + "'))" _
                                        + ",'" + cursor2 + "'" _
                                    + ");"
            Command.ExecuteNonQuery()
            Do
                Command.CommandText = "FETCH FORWARD " + cursor_chunk.ToString + " FROM """ + cursor2 + """;"
                Reader = Command.ExecuteReader
                sbResult.Clear()
                If Reader.HasRows Then
                    While Reader.Read()
                        sbResult.Append(Reader.Item(0).ToString)
                    End While
                    Reader.Close()
                    If counter Mod chunk = 0 Then
                        File.AppendAllText(FILEPATH + sFileName, "</cars245>")
                        part_counter += 1
                        sFileName = "b2b_v3_and_product_data_sync_" + sync_date + "_part" + part_counter.ToString() + ".xml"
                        File.WriteAllText(FILEPATH + sFileName, "<?xml version=""1.0"" encoding=""UTF-8""?><cars245>")
                    End If
                    counter += cursor_chunk
                    File.AppendAllText(FILEPATH + sFileName, sbResult.ToString)
                Else
                    Reader.Close()
                    Command.CommandText = "COMMIT;"
                    Command.ExecuteNonQuery()
                    Command.Cancel()
                    Command.Connection.Close()
                    File.AppendAllText(FILEPATH + sFileName, "</cars245>")
                    Exit Do
                End If
            Loop
            'b2b v3.0 ------------------------------------------------------------------------------------------------

            'b2b v3.0 config------------------------------------------------------------------------------------------
            sbResult.Clear()
            Command.Connection = New NpgsqlConnection(CONNECTIONSTRING)
            Command.Connection.Open()
            Command.CommandText = " select * from cars245.gen_b2b_v3_all_presets_xml();"
            Reader = Command.ExecuteReader
            If Reader.HasRows Then
                While Reader.Read()
                    sbResult.Append(Reader.Item(0).ToString)
                End While
                Reader.Close()
            End If
            Command.Cancel()
            Command.Connection.Close()
            sFileName = "b2b_v3_data_all_" + sync_date + ".xml"
            File.AppendAllText(FILEPATH + sFileName, sbResult.ToString)
            'b2b v3.0 config -----------------------------------------------------------------------------------------



            MsgBox("ws_daily_sync - done")
            tb_chunkSize_ws.Enabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message & vbCrLf & ex.ToString)
        End Try


    End Sub


End Class
