﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btn_Get_Config = New System.Windows.Forms.Button()
        Me.CheckedListBox1 = New System.Windows.Forms.CheckedListBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.tb_chunkSize_ws = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.ListBoxBrandsInfoBlock = New System.Windows.Forms.ListBox()
        Me.ListBoxClientsInfoBlock = New System.Windows.Forms.ListBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.CheckedListBoxSlugs = New System.Windows.Forms.CheckedListBox()
        Me.CheckBox_del = New System.Windows.Forms.CheckBox()
        Me.btn_ws_delete_client_discounts = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btn_ws_generate = New System.Windows.Forms.Button()
        Me.CheckedListBoxClients = New System.Windows.Forms.CheckedListBox()
        Me.btn_ws_remove_selection = New System.Windows.Forms.Button()
        Me.btn_ws_select_all = New System.Windows.Forms.Button()
        Me.get_ws_list = New System.Windows.Forms.Button()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label_chunkSize = New System.Windows.Forms.Label()
        Me.tb_chunkSize = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage2.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(179, 49)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(158, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Result in ""C:\TDR\Export files"":"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(10, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = " "
        '
        'btn_Get_Config
        '
        Me.btn_Get_Config.Location = New System.Drawing.Point(22, 10)
        Me.btn_Get_Config.Name = "btn_Get_Config"
        Me.btn_Get_Config.Size = New System.Drawing.Size(140, 22)
        Me.btn_Get_Config.TabIndex = 3
        Me.btn_Get_Config.Text = "Get config"
        Me.btn_Get_Config.UseVisualStyleBackColor = True
        '
        'CheckedListBox1
        '
        Me.CheckedListBox1.CheckOnClick = True
        Me.CheckedListBox1.FormattingEnabled = True
        Me.CheckedListBox1.Location = New System.Drawing.Point(381, 39)
        Me.CheckedListBox1.Name = "CheckedListBox1"
        Me.CheckedListBox1.Size = New System.Drawing.Size(156, 349)
        Me.CheckedListBox1.TabIndex = 4
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(381, 10)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 22)
        Me.Button3.TabIndex = 5
        Me.Button3.Text = "all"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(462, 10)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 22)
        Me.Button4.TabIndex = 6
        Me.Button4.Text = "none"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Enabled = False
        Me.Button5.Location = New System.Drawing.Point(182, 366)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(155, 22)
        Me.Button5.TabIndex = 7
        Me.Button5.Text = "Gen. all global discounts"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.tb_chunkSize_ws)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.Button6)
        Me.TabPage2.Controls.Add(Me.ListBoxBrandsInfoBlock)
        Me.TabPage2.Controls.Add(Me.ListBoxClientsInfoBlock)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.Button8)
        Me.TabPage2.Controls.Add(Me.Button7)
        Me.TabPage2.Controls.Add(Me.CheckedListBoxSlugs)
        Me.TabPage2.Controls.Add(Me.CheckBox_del)
        Me.TabPage2.Controls.Add(Me.btn_ws_delete_client_discounts)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.btn_ws_generate)
        Me.TabPage2.Controls.Add(Me.CheckedListBoxClients)
        Me.TabPage2.Controls.Add(Me.btn_ws_remove_selection)
        Me.TabPage2.Controls.Add(Me.btn_ws_select_all)
        Me.TabPage2.Controls.Add(Me.get_ws_list)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(963, 414)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "ws + product sync: v1 & v2 & v3"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'tb_chunkSize_ws
        '
        Me.tb_chunkSize_ws.Location = New System.Drawing.Point(273, 363)
        Me.tb_chunkSize_ws.Name = "tb_chunkSize_ws"
        Me.tb_chunkSize_ws.Size = New System.Drawing.Size(69, 20)
        Me.tb_chunkSize_ws.TabIndex = 23
        Me.tb_chunkSize_ws.Text = "50000"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(270, 336)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(61, 13)
        Me.Label7.TabIndex = 22
        Me.Label7.Text = "chunk_size"
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button6.Location = New System.Drawing.Point(27, 325)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(140, 24)
        Me.Button6.TabIndex = 21
        Me.Button6.Text = "sync all (v1/v2/v3)"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'ListBoxBrandsInfoBlock
        '
        Me.ListBoxBrandsInfoBlock.FormattingEnabled = True
        Me.ListBoxBrandsInfoBlock.HorizontalScrollbar = True
        Me.ListBoxBrandsInfoBlock.Location = New System.Drawing.Point(769, 81)
        Me.ListBoxBrandsInfoBlock.Name = "ListBoxBrandsInfoBlock"
        Me.ListBoxBrandsInfoBlock.Size = New System.Drawing.Size(164, 303)
        Me.ListBoxBrandsInfoBlock.TabIndex = 20
        '
        'ListBoxClientsInfoBlock
        '
        Me.ListBoxClientsInfoBlock.FormattingEnabled = True
        Me.ListBoxClientsInfoBlock.HorizontalScrollbar = True
        Me.ListBoxClientsInfoBlock.Location = New System.Drawing.Point(386, 226)
        Me.ListBoxClientsInfoBlock.Name = "ListBoxClientsInfoBlock"
        Me.ListBoxClientsInfoBlock.Size = New System.Drawing.Size(156, 160)
        Me.ListBoxClientsInfoBlock.TabIndex = 19
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(602, 55)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(163, 13)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Select if need specific brand only"
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(688, 20)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(75, 22)
        Me.Button8.TabIndex = 17
        Me.Button8.Text = "none"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(599, 20)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 22)
        Me.Button7.TabIndex = 16
        Me.Button7.Text = "all"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'CheckedListBoxSlugs
        '
        Me.CheckedListBoxSlugs.CheckOnClick = True
        Me.CheckedListBoxSlugs.FormattingEnabled = True
        Me.CheckedListBoxSlugs.Location = New System.Drawing.Point(599, 81)
        Me.CheckedListBoxSlugs.Name = "CheckedListBoxSlugs"
        Me.CheckedListBoxSlugs.Size = New System.Drawing.Size(164, 304)
        Me.CheckedListBoxSlugs.TabIndex = 15
        '
        'CheckBox_del
        '
        Me.CheckBox_del.AutoSize = True
        Me.CheckBox_del.Location = New System.Drawing.Point(173, 369)
        Me.CheckBox_del.Name = "CheckBox_del"
        Me.CheckBox_del.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox_del.TabIndex = 14
        Me.CheckBox_del.UseVisualStyleBackColor = True
        '
        'btn_ws_delete_client_discounts
        '
        Me.btn_ws_delete_client_discounts.Enabled = False
        Me.btn_ws_delete_client_discounts.Location = New System.Drawing.Point(27, 363)
        Me.btn_ws_delete_client_discounts.Name = "btn_ws_delete_client_discounts"
        Me.btn_ws_delete_client_discounts.Size = New System.Drawing.Size(140, 25)
        Me.btn_ws_delete_client_discounts.TabIndex = 13
        Me.btn_ws_delete_client_discounts.Text = "delete client discounts"
        Me.btn_ws_delete_client_discounts.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(184, 81)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(16, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "..."
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(184, 55)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(158, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Result in ""C:\TDR\Export files"":"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(184, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(114, 13)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "id 2808 - our test client"
        '
        'btn_ws_generate
        '
        Me.btn_ws_generate.Enabled = False
        Me.btn_ws_generate.Location = New System.Drawing.Point(27, 55)
        Me.btn_ws_generate.Name = "btn_ws_generate"
        Me.btn_ws_generate.Size = New System.Drawing.Size(140, 22)
        Me.btn_ws_generate.TabIndex = 8
        Me.btn_ws_generate.Text = "Ready Steady Go!"
        Me.btn_ws_generate.UseVisualStyleBackColor = True
        '
        'CheckedListBoxClients
        '
        Me.CheckedListBoxClients.CheckOnClick = True
        Me.CheckedListBoxClients.FormattingEnabled = True
        Me.CheckedListBoxClients.Location = New System.Drawing.Point(386, 55)
        Me.CheckedListBoxClients.Name = "CheckedListBoxClients"
        Me.CheckedListBoxClients.Size = New System.Drawing.Size(156, 169)
        Me.CheckedListBoxClients.TabIndex = 8
        '
        'btn_ws_remove_selection
        '
        Me.btn_ws_remove_selection.Location = New System.Drawing.Point(467, 20)
        Me.btn_ws_remove_selection.Name = "btn_ws_remove_selection"
        Me.btn_ws_remove_selection.Size = New System.Drawing.Size(75, 22)
        Me.btn_ws_remove_selection.TabIndex = 10
        Me.btn_ws_remove_selection.Text = "none"
        Me.btn_ws_remove_selection.UseVisualStyleBackColor = True
        '
        'btn_ws_select_all
        '
        Me.btn_ws_select_all.Location = New System.Drawing.Point(386, 20)
        Me.btn_ws_select_all.Name = "btn_ws_select_all"
        Me.btn_ws_select_all.Size = New System.Drawing.Size(75, 22)
        Me.btn_ws_select_all.TabIndex = 9
        Me.btn_ws_select_all.Text = "all"
        Me.btn_ws_select_all.UseVisualStyleBackColor = True
        '
        'get_ws_list
        '
        Me.get_ws_list.Location = New System.Drawing.Point(27, 20)
        Me.get_ws_list.Name = "get_ws_list"
        Me.get_ws_list.Size = New System.Drawing.Size(140, 22)
        Me.get_ws_list.TabIndex = 8
        Me.get_ws_list.Text = "get wholesalers id list"
        Me.get_ws_list.UseVisualStyleBackColor = True
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label_chunkSize)
        Me.TabPage1.Controls.Add(Me.tb_chunkSize)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Button5)
        Me.TabPage1.Controls.Add(Me.Button4)
        Me.TabPage1.Controls.Add(Me.Button1)
        Me.TabPage1.Controls.Add(Me.Button3)
        Me.TabPage1.Controls.Add(Me.btn_Get_Config)
        Me.TabPage1.Controls.Add(Me.CheckedListBox1)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(963, 414)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Product Data sync"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label_chunkSize
        '
        Me.Label_chunkSize.AutoSize = True
        Me.Label_chunkSize.Location = New System.Drawing.Point(179, 20)
        Me.Label_chunkSize.Name = "Label_chunkSize"
        Me.Label_chunkSize.Size = New System.Drawing.Size(64, 13)
        Me.Label_chunkSize.TabIndex = 9
        Me.Label_chunkSize.Text = "chunk_size:"
        '
        'tb_chunkSize
        '
        Me.tb_chunkSize.Location = New System.Drawing.Point(266, 20)
        Me.tb_chunkSize.Name = "tb_chunkSize"
        Me.tb_chunkSize.Size = New System.Drawing.Size(71, 20)
        Me.tb_chunkSize.TabIndex = 8
        Me.tb_chunkSize.Text = "50000"
        '
        'Button1
        '
        Me.Button1.Enabled = False
        Me.Button1.Location = New System.Drawing.Point(22, 40)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(140, 22)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "product data sync"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(971, 440)
        Me.TabControl1.TabIndex = 8
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(995, 464)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btn_Get_Config As System.Windows.Forms.Button
    Friend WithEvents CheckedListBox1 As System.Windows.Forms.CheckedListBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents get_ws_list As System.Windows.Forms.Button
    Friend WithEvents btn_ws_remove_selection As System.Windows.Forms.Button
    Friend WithEvents btn_ws_select_all As System.Windows.Forms.Button
    Friend WithEvents CheckedListBoxClients As System.Windows.Forms.CheckedListBox
    Friend WithEvents btn_ws_generate As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents CheckBox_del As System.Windows.Forms.CheckBox
    Friend WithEvents btn_ws_delete_client_discounts As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents CheckedListBoxSlugs As System.Windows.Forms.CheckedListBox
    Friend WithEvents ListBoxClientsInfoBlock As System.Windows.Forms.ListBox
    Friend WithEvents ListBoxBrandsInfoBlock As System.Windows.Forms.ListBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents tb_chunkSize As System.Windows.Forms.TextBox
    Friend WithEvents Label_chunkSize As System.Windows.Forms.Label
    Friend WithEvents tb_chunkSize_ws As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label

End Class
