﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;


namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string description = richTextBox1.Text;
            string html = "";
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(description);
            if (doc.DocumentNode.SelectNodes("li") != null )
            {
                foreach (HtmlNode htmlnodes in doc.DocumentNode.Descendants("li"))
                {
                    if (html.Length == 0)
                    {
                        html = htmlnodes.InnerHtml;
                    }
                    else
                    {
                        html = html + "</br>" + htmlnodes.InnerHtml;
                    }
                }
            }
            richTextBox2.Text = html;
            description = html;
        }
            
     
    
    }
}
